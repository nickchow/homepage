// page init
jQuery(function() {
  scroller.init();
});

// move some elements on scroll
var scroller = {
  config : {
    docElem : window.document.documentElement,
    // The viewportFactor defines how much of the appearing item has to be visible in order to trigger the animation
    // if we'd use a value of 0, this would mean that it would add the animation class as soon as the item is in the viewport. 
    // If we were to use the value of 1, the animation would only be triggered when we see all of the item in the viewport (100% of it)
    viewportFactor : 0,
    elems : '.slide-holder'
  },
  getViewportH : function() {
    var client = this.config.docElem['clientHeight'],
      inner = window['innerHeight'];
    
    if( client < inner )
      return inner;
    else
      return client;
  },
  scrollY : function() {
    return window.pageYOffset || this.config.docElem.scrollTop;
  },
  getOffset : function(el) {
    var offsetTop = 0, offsetLeft = 0;
    do {
      if ( !isNaN( el.offsetTop ) ) {
        offsetTop += el.offsetTop;
      }
      if ( !isNaN( el.offsetLeft ) ) {
        offsetLeft += el.offsetLeft;
      }
    } while( el = el.offsetParent )

    return {
      top : offsetTop,
      left : offsetLeft
    }
  },
  inViewport : function(el, h) {
    var elH = el.offsetHeight,
      scrolled = this.scrollY(),
      viewed = scrolled + this.getViewportH(),
      elTop = this.getOffset(el).top,
      elBottom = elTop + elH,
      // if 0, the element is considered in the viewport as soon as it enters.
      // if 1, the element is considered in the viewport only when it's fully inside
      // value in percentage (1 >= h >= 0)
      h = h || 0;

    return (elTop + elH * h) <= viewed && (elBottom) >= scrolled;
  },
  init : function() {
    this.sections = jQuery(this.config.elems);
    this.didScroll = false;

    var self = this,
      scrollHandler = function() {
        if( !self.didScroll ) {
          self.didScroll = true;
          setTimeout( function() { self.scrollPage(); }, 60 );
        }
      },
      resizeHandler = function() {
        function delayed() {
          self.scrollPage();
          self.resizeTimeout = null;
        }
        if ( self.resizeTimeout ) {
          clearTimeout( self.resizeTimeout );
        }
        self.resizeTimeout = setTimeout( delayed, 200 );
      };

    window.addEventListener( 'scroll', scrollHandler, false );
    window.addEventListener( 'resize', resizeHandler, false );
  },
  scrollPage : function() {
    var self = this;

    this.sections.each(function() {
      if( self.inViewport( this, self.config.viewportFactor ) ) {
        jQuery(this).addClass('scroll-effect-animate');
      }
      else {
        // this add class init if it doesn't have it. This will ensure that the items initially in the viewport will also animate on scroll
        jQuery(this).addClass('scroll-effect-init');
        jQuery(this).removeClass('scroll-effect-animate');
      }
    });
    this.didScroll = false;
  }
}