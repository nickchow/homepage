$(function() {

  // clicking the "up" button will make the page scroll to the top of the page
  $('#scroll_up').click(
    function (e) {
      $('html, body').animate({scrollTop: '0px'}, 800);
    }
  );
 });